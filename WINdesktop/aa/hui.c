#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
typedef struct TTree
{
	int value;
	struct TTree* left;
	struct TTree* right;
}Tree;

int contains(const Tree* tree, int value)
{
	if (tree ==NULL)
	{
		return 0;
	}
	if (tree->value == value)
	{
		return 1;
	}
}

int sum(int* arr, int n)
{
	int res;
	for (int i=0;i<n;i++)
	{
		res+=arr[i];
	}
	return res;
}

Tree* insert(Tree* tree, int value)
{
	if(tree==NULL)
	{
		Tree* newTree = malloc(sizeof(Tree));
		newTree->value = value;
		newTree->left = NULL;
		newTree->right = NULL;
		return newTree;
	}
	if(tree->value == value)
	{
		return tree;
	}
	if (value < tree->value)
	{
		tree->left = insert(tree->left,value);
	}
	else
	{
		tree->right = insert(tree->right,value);
	}
}

void printInorder(const Tree* tree, int tab)
{
	if (tree ==NULL){
		return;
	}
	printInorder(tree->left, tab+2);
	printInorder(tree->right, tab+2);
	for (int i=0; i<tab;i++)
	{
		putchar(' ');
	}
}
int main()
{
	Tree* root=NULL;
	int value;
	while(scanf("%d",&value) == 1)
	{
		root = insert(root, value);
		puts("---------------");
		printInorder(root, 0);
		puts("---------------");
	}
}


//~ int main(int argc, char **argv)
//~ {
	//~ int n;
	//~ scanf("%d",&n);
	//~ int* arr=malloc(n*sizeof(int));
	//~ for (int i=0;i<n;i++)
	//~ {
		//~ scanf("%d",&arr[i]);
	//~ }
	//~ int s=sum(arr,n);
	//~ printf("%d",s);
	//~ return 0;
//~ }

