#!/bin/bash
length=10
for i in $@ 
	do
		if [[ $(echo $i | cut -d '=' -f 1) -eq 'length' ]]
		then
			length=$2
		fi
	done
for i in $(ls "$1"*)
do
	if [[ $(echo -n $i | wc -c | cut -d ' ' -f 1) < $length ]]
	then
		rm -r $i
	fi
done

