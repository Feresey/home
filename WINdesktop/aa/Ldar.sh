#!/bin/bash
if [[ $# = 0 ]]
then
	echo "NO argument"
	exit 2
fi
while [ 1 ]
do
	sum=0
	for i in $(ls "$1"*)
	do
		sum=$(($sum + $(stat -c %s $i))) 
	done
	echo "Current size: "$sum
	if [[ $sum > $2 ]]
	then
		rm  $(echo $(ls -S "$1"*) | cut -d ' ' -f 1) 
	else
		echo done
		break
	fi
done
