#!/bin/bash
n=$(find -size 1c)
res=lol
ind=1
for i in #@ 
do
	if [[ -f $i && $(echo $i | cut -d '=' -f 1) !=  'res' && $(stat -c %s $i) = 1 ]] 
	then
		if [ $ind ] 
		then 
			n=$n+$i
		else
			ind=0
			n=$i
		fi
	else
		res=$(echo $i | cut -d '=' -f 2)
	fi
done

if [[ $n = '' ]]
then
	echo "No inimal length files!"
	exit 1
fi
echo -n '' > $res
echo "Output file is >> $res"

while [ 1=1 ] 
	do
	for i in $n 
	do
		if [[ $(stat -c %s $res) -lt 4096 ]]
		then	
			echo -n $(cat $i) >> $res
		else
			echo "Program completed!"
			exit 0
		fi
	done
done
