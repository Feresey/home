#!/bin/bash
ind=false
res=lol
for i in $@
do
	if [[ $(echo -n $i | cut -d '=' -f 1) = 'res' ]]
	then
		res=$(echo -n $i | cut -d '=' -f 2)
		n=$i
	fi
done
echo "Name of output file is >> $res"i $6
echo $#
if [ ! -f $res ] 
then
	touch $res
fi

if [[ $(stat $res -c %s) = 0 ]]
then
	echo -n "1" > $res
fi

while [[ $(stat $res -c %s) -lt 4096 ]]
do
	for i in $@
	do
		count=1
		if [[ "$i" -ne "$n" && -f $i && $(stat $i -c %s) = 1 ]]
		then
			echo -n $i >> $res
			ind=true
		fi

		if [[ "$count" -eq  && "$ind" -eq "false" ]]
		then
			echo "Warning! No minimal length files!!!"
			exit 1
		fi
	
			done
done

exit 0

