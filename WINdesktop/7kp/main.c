#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
int m, n;

void rid(FILE *in)
{
	int ind, first, second;
	while(!feof(in))
	{
		fscanf(in, "%d %d", &first, &second);
		ind = 0;
		if(first == 0 && second > m)
		{
			m = second;
			ind = 1;
		}
		if (ind == 0 && first > n)
			n = first;
	}
	
}

int main()
{
	FILE *in;
	in = fopen("input2.txt","r");
	rid(in);
	fseek(in, 0, SEEK_SET);
	int A[m][n], str = 0, ind, first, second, max = 0, jj = 0, jjprev = 0;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
			A[i][j] = 0;
	}
	while(!feof(in))
	{
		fscanf(in, "%d %d", &first, &second);
		ind = 0;
		if(first == 0)
		{
			if (second != 0)
				str = second - 1;
			ind = 1;
		}
		if (ind == 0)
			A[str][first -1] = second;
	}
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
			printf("%5d", A[i][j]);
		printf("\n");
	}
	
	
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if ( abs(A[i][j]) > abs(max)) 
			{
				max = A[i][j];
				jj = j;
				jjprev = 0;
			}
			else if (abs(A[i][j]) == max)
			{
				jjprev = jj;
				jj = j;
			}
		}
	}
	
	
	printf("max = %d\n", max);
	if (jjprev == 0) jjprev = jj;
	for (int l = 0; l < n; l++)
	{
		A[l][jjprev] = A[l][jjprev] / max;
	}
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
			printf("%5d", A[i][j]);
		printf("\n");
	}
	fclose(in);
	return 0;
}
