#include "deck.h"

int N = 1000, ind = 1;

deck* init(){
	deck* X = (deck*)malloc(sizeof(deck));
	X->arr = (int*)malloc(sizeof(int)*N);
	X->start = N/2;
	X->end = N/2 - 1;
	return X;
}

int isEmpty(deck* X){
	if (X->start == X->end + 1)
	{
		puts("Can't print deck: deck is empty");
		return 1;
	}
	return 0;
}

void prntDc(deck* X){
	if(!isEmpty(X))
	{
		for (int i = X->start; i <= X->end; i++)
			printf("%5d", X->arr[i]);
		puts("");
	}
	return;
}

deck* pushStart(deck* X, int value){
	X->arr[X->start - 1] = value;
	X->start -= 1;
	return X;
}

deck* pushEnd(deck* X, int value){
	X->arr[X->end + 1] = value;
	X->end += 1;
	return X;
}

int popStart(deck* X){
	ind = 1;
	if (isEmpty(X))
	{
		ind = 0;
		return 1;
	}
	return X->arr[X->start++];
}

int popEnd(deck* X){
	ind = 1;
	if (isEmpty(X))
	{
		ind = 0;
		return 0;
	}
	return X->arr[X->end--];
}

deck* clear(deck* X){
	if (isEmpty(X))
	{
		return X;
	}
	else
	{
		X->start = N/2;
		X->end = N/2 - 1;
		puts("Can't print deck: deck is empty");
	}
	return X;
}

void showFirst(deck* X){
	if (!isEmpty(X)) printf("First = %d\n", X->arr[X->start]);
	return;
}

void showEnd(deck* X){

	if (!isEmpty(X)) printf("End = %d\n", X->arr[X->end]);
	return;
}

deck* createDeck()
{
	deck* D = init();
	int pop = 0;
	char q[20];
	int value = 0;
	printf(">> ");
	while(scanf("%s", q) == 1)
	{
		if(q[0] == '+')
		{
			scanf("%d", &value);
			if (q[1] == 's')
			{
				D = pushStart(D, value);
				prntDc(D);
			}
			else if (q[1] == 'e')
			{
				D = pushEnd(D, value);
				prntDc(D);
			}
		}
		else if(q[0] == '-')
		{
			if (q[1] == 'e')
			{
				pop = popEnd(D);
				if (ind)
				{
					printf("popE = %d\t\t", pop);
					prntDc(D);
				}
			}
			else if (q[1] == 's')
			{
				pop = popStart(D);
				if (ind)
				{
					printf("popS = %d\t\t", pop);
					prntDc(D);
				}
			}
			else if (q[1] == 'a')
			{
				D = clear(D);
				if (!ind) prntDc(D);
			}
		}
		else if(q[0] == 's')
		{
			if (q[1] == 'f')
				showFirst(D);
			else if(q[1] == 'e')
				showEnd(D);
			else if (q[1] == 'a')
				prntDc(D);
		}
		else
		{
			puts("Deck is complete!");
			break;
		}
		printf(">> ");
	}
	return D;
}
