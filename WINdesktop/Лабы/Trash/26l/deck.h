#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef __deck_h__
#define __deck_h__

typedef struct dk{
	int *arr;
	int start;
	int end;
} deck;

deck* init();

int isEmpty(deck* X);

void prntDc(deck* X);

deck* pushStart(deck* X, int value);

deck* pushEnd(deck* X, int value);

int popStart(deck* X);

int popEnd(deck* X);

deck* clear(deck* X);

void showFirst(deck* X);

void showEnd(deck* X);

deck* createDeck();

#endif
