#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
int m, n;
void prnt(FILE* X, int a, int b)
{
	fprintf(X, "%d %d ", a, b);
	fprintf(stdout, "%d %d ", a, b);
	return;
}
void rid(FILE *in)
{
	fseek(in, 0, SEEK_SET);
	int ind, first, second;
	while(fscanf(in, "%d %d", &first, &second) == 2)
	{
		ind = 0;
		if(first == 0 && second > m)
		{
			m = second;
			ind = 1;
		}
		if (ind == 0 && first > n)
			n = first;
	}
	return;
}

void fill(FILE *in, int A[m][n])
{
	fseek(in, 0, SEEK_SET);
	int str = 0, ind, first, second;
	printf("Short matrix = ");
	while(fscanf(in, "%d %d", &first, &second) == 2)
	{
		
		printf("%d %d ", first, second);
		ind = 0;
		if(first == 0)
		{
			if (second != 0)
				str = second - 1;
			ind = 1;
		}
		if (ind == 0)
			A[str][first -1] = second;
	}
	puts("\n");
	return;
}

int main(int argc, char **argv)
{
	FILE *in, *out;
	if (argc - 1 != 0)
		in = fopen(argv[1],"r");
	else {
		puts("Can\'t input");
		return 1;
	}
	out = fopen("output.txt", "w");
	rid(in);
	int A[m][n], first, second, max = 0, jj = 0, jjprev = 0;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
			A[i][j] = 0;
	}
	fill(in, A);

	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
			printf("%5d", A[i][j]);
		printf("\n");
	}
	
	
	fseek(in, 0, SEEK_SET);
	while(fscanf(in, "%d %d", &first, &second) == 2) //find max 
	{
		if (first != 0)
		{
			if (abs(max) < abs(second))
			{
				max = second;
				jj = first;
				jjprev = 0;
			}
			else if (abs(max) == abs(second))
			{
				max = second;
				jjprev = jj;
				jj = first;
			}
		}
	}
	if (jjprev == 0) jjprev = jj;
	printf("max = %d\nShort matrix = ", max);
	
	
	fseek(in, 0, SEEK_SET);
	while(fscanf(in, "%d %d", &first, &second) == 2){
		if (first == jjprev)
		{
			second /= max;
			if (second != 0)
				prnt(out, first, second);
		}
		else
		printf("%d %d ", first, second);
	}
	puts("\n");
	fclose(out);
	out = fopen("output.txt", "r");
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n; j++)
			A[i][j] = 0;
	}
	fill(in, A);
	for (int i = 0; i < m; i++) //Вывод матрицы
	{
		for (int j = 0; j < n; j++)
			printf("%5d", A[i][j]);
		printf("\n");
	}
	fclose(in);
	fclose(out);
	return 0;
}
