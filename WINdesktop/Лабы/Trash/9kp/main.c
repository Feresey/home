#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int n  = -1;
typedef struct 
{
	int key;
	char string[100];
} line;

line L[100];

void prnt()
{
	puts("\nKey: \t\tString:");
	for (int i = 0; i<n; i++)
		printf("%6d│ %s", L[i].key, L[i].string);
}

void randomize()
{
	long a = time(0);
	srand(a);
}

void randLines()
{
	randomize();
	line temp;
	for (int i = 0; i<n; i++)
	{
		int j = rand() % n;
		temp = L[i];
		L[i] = L[j];
		L[j] = temp;
	}
}

void revers()
{
	line temp;
	for (int i = 0; i<n / 2; i++)
	{
		temp = L[i];
		L[i] = L[n - i - 1];
		L[n - i - 1] = temp;
	}
}

void bsrch(int k)
{
	int m, left = 0, right = n - 1;
	while (left < right)
	{
		m = (left + right) / 2;
		if (k > L[m].key)
			left = m + 1;
		else  right = m;
	}
	if (k == L[left].key)
		printf("Key is found:\n%d %s", k, L[left].string);
	else puts("Key not found");
}

void title()
{
	puts("Commands:\n\
		1:\t Print file\n\
		2:\t Sort file\n\
		3:\t Binary search\n\
		4:\t Randomize lines\n\
		5:\t Reverse lines\n\
		0:\t Exit");
	return;
}

void sort()
{
	int i,j,min;
	line temp;
	for (i = 0; i < n - 1; i++)
	{
		min = i; 
		for (j = i + 1; j < n; j++)
		{
			if (L[j].key < L[min].key)
			min = j;
		}
		temp = L[i];
		L[i] = L[min];
		L[min] = temp;
	}
	return;
}

int main(int argc, char **argv)
{
	FILE *in;
	int input, k = 0, ind = 0;
	
	if (argc > 1)
	{
		if(!(in = fopen(argv[1],"r")))
		{
			puts("Can\'t open");
			return 1;
		}
	}
	else if(!(in = fopen("in1.txt","r")))
	{
		puts("Can\'t open");
		return 1;
	}
	
	while (!feof(in))
	{
		fscanf(in, "%d", &L[++n].key);
		fgets(L[n].string, 100, in);
	}
	title();
	printf(">> ");
	
	while (scanf("%d",  &input) == 1)
	{
		switch(input)
		{
			case 1:
				prnt();
				break;
			case 2:
				sort();
				prnt();
				ind = 1;
				break;
			case 3:
				if (ind)
				{
					printf("Enter the key:");
					scanf("%d", &k);
					bsrch(k);
				}
				else puts("Sort file first");
				break;
			case 4:
				randLines();
				prnt();
				break;
			case 5:
				revers();
				prnt();
				break;
			case 0:
				fclose(in);
				return 0;
			default:
				puts("Wrong command");
				break;
		}
		title();
		printf(">> ");
	}
	return 0;
}
