#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct TTree {
  int value;
  struct TTree* child;
  struct TTree* sibling;

} Tree;

Tree* createNode(int value) {
  Tree* node = (Tree*)malloc(sizeof(Tree));
  node->value = value;
  node->child = NULL;
  node->sibling = NULL;
  return node;
}

Tree* createRoot(int value) {
  return createNode(value);
}

Tree* addSibling(Tree* tree, int value) {
  if(tree->sibling == NULL){
    tree->sibling = createNode(value);
    return tree;
  }
  tree->sibling = addSibling(tree->sibling, value);
  return tree;
}

Tree* addChild(Tree* tree, int value) {
  if(tree->child == NULL){
    tree->child = createNode(value);
    return tree;
  }
  tree->child = addSibling(tree->child, value);
  return tree;
}

Tree* insert(Tree* tree, const char* path, int value) {
  if(*path == '\0'){
    addChild(tree, value);
    return tree;
  }
  if(*path == 'c'){
    tree->child = insert(tree->child, path + 1, value);
  }else{
    tree->sibling = insert(tree->sibling, path + 1, value);
  }
  return tree;
}

void printTree(const Tree* tree, int tab) {
  if(tree == NULL){
    return;
  }
  for(int i = 0; i < tab; ++i){
    putchar(' ');
  }
  printf("%d\n", tree->value);
  printTree(tree->child, tab + 2);
  printTree(tree->sibling, tab);
}

void eraseThis(Tree* tree) {
  if(tree == NULL){
    return;
  }
  eraseThis(tree->child);
  eraseThis(tree->sibling);
  free(tree);
}

Tree* erase(Tree* tree, const char* path) {
  if(*path == '\0'){
    eraseThis(tree->child);
    Tree* temporary = tree->sibling;
    free(tree);
    return temporary;
  }
  if(*path == 'c'){
    tree->child = erase(tree->child, path + 1);
  }else{
    tree->sibling = erase(tree->sibling, path + 1);
  }
  return tree;
}

int max(int a, int b) {
  if(a < b){
    return b;
  }
  return a;
}

int calcSiblings(const Tree* tree) {
  if(tree == NULL){
    return 0;
  }
  return calcSiblings(tree->sibling) + 1;
}

int calcDegree(const Tree* tree) {
  if(tree == NULL){
    return 0;
  }
  int thisDegree = calcSiblings(tree->child);
  int childDegree = calcDegree(tree->child);
  int siblingDegree = calcDegree(tree->sibling);
  return max(thisDegree, max(childDegree, siblingDegree));
}

int main() {
  Tree* root = NULL;
  int value;
  char q[10];
  while(scanf("%s%d", q, &value) == 2){
    if(q[0] == 'r'){
      root = createRoot(value);
    }else if(q[0] == '+'){
      root = insert(root, q + 1, value);
      puts("---------------");
      printTree(root, 0);
      puts("---------------");
    }else if(q[0] == '-'){
      root = erase(root, q + 1);
      puts("---------------");
      printTree(root, 0);
      puts("---------------");
    }else if(q[0] == 'q'){
      int degree = calcDegree(root);
      printf("%d\n", degree);
    }
  }
}
