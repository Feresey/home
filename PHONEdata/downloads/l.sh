#!/bin/bash 
if [[ $# = 0 ]] 
then 
echo "NO argument" 
exit 2 
fi 
while [ 1 ] 
do 
	sum=0 
	for i in $(ls) 
	do 
		if [ -f $i ]
		then
			sum=$(($sum + $(stat -c %s $i))) 
		fi
	done 
	sumpref=0
	for i in $(find . -name "$1"'*') 
	do 
		if [ -f $i ]
		then
			sumpref=$(($sumpref + $(stat -c %s $i))) 
		fi
	done 
	
	echo "Current size: "$sum 
	if [[  $sumpref -lt $(($sum-$2)) ]] 
	then 
		rm -vi $(find . -name $1'*' -exec ls -Ss {} \; |sort -h | tail -n 1|cut -d ' ' -f 2)
	else 
		echo done 
		break 
	fi 
done
