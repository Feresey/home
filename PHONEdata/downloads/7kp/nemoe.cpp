#include <stdio.h>
void prn_matrix(int **, int, int);

void prn_sh_mtrx(int *, int*, int);

void ftosh(int**, int, int, int*, int*, int&);

void shtof(int*, int*, int, int**, int, int);

void mult(int**, int*, int*, int, int);

void prn_vect(int*, int);

void multsh(int*, int*, int&, int*, int*, int, int);

int main(){
	int **mtr, *a, *ind, k, n, m, i,//input matr
				*vect1,//vector
				*vect2;//result vector
	 	printf("Input number of strings:\n n=\n"); scanf("%d",&n);
		printf("Input number of columns:\n m=\n"); scanf("%d",&m);
		mtr=new int*[n];
		for(i = 0; i < n; i++)
		{
			mtr[i] = new int[m];
		}
		vect1=new int[m];

		vect2=new int[n];

		printf("Input number of elements, which are not 0:\n k=\n"); scanf("%d",&k);
		a=new int[k];
		ind=new int[k];
	 
	for(i = 0; i < k; i++)
	{ 
	 scanf("%d",&a[i]);
	 scanf("%d",&ind[i]); 
	}
	for (i=0;i<m;i++)
	{
	 scanf("%d",&vect1[i]);
	}
	printf("Short matrix:\n");
	prn_sh_mtrx(a, ind, k);
	printf("Vector:\n");
	prn_vect(vect1,m);
	multsh(a, ind, k,vect1,vect2, n, m);
	printf("Result with short matrix:\n");
	prn_vect(vect2,n);
	printf("Full matrix:\n");
	shtof(a, ind, k, mtr, n, m);
	prn_matrix(mtr,n,m);
	printf("Result with full matrix:\n");
	mult(mtr,vect1,vect2,n,m);
	prn_vect(vect2,n);
	int nl=0;
	for(int p=0;p<n;p++)
	{
		if(vect2[p])
		{
			nl++;
		}
	}
	printf("Number of elements of output vector, which are not 0:\n%d\n",nl);

}
void ftosh(int ** mtr, int n, int m, int*a, int *ind, int &k)
{ 
	int i,j; k=0;
	for(i = 0; i < n; i++)
	for(j = 0; j < m; j++)
	if(mtr[i][j] != 0) 
		{
			a[k]=mtr[i][j]; 
			ind[k]=i*m+j; 
			k++; 
		}
}
void prn_vect(int *vect, int size_x)
{
	int x;
	for (x=0;x<size_x;x++)
	{
		printf("\n%3d\n",vect[x]);
	}
}
void prn_matrix(int **matrix, int size_x, int size_y)
{
		int x,y=0;
		for( x = 0; x < size_x; x++)
		{
			printf("\n");
			for( y = 0;y < size_y; y++)
			printf("%3d ", matrix[x][y]);
			printf("\n");
		}  
}

void shtof(int* a, int* ind, int k, int** mtr, int n, int m)
{ 
	int i, j, l;
	for(i = 0; i < n; i++)
	 	{
	 		for(j = 0; j < m; j++)
	 		{
				mtr[i][j]=0;
	 		}
	 	}
	for(l = 0; l < k; l++)
		{ 
			i=ind[l]/m; 
			j=ind[l]%m; 
			mtr[i][j]=a[l]; 
		}
}

void prn_sh_mtrx(int *a, int *ind, int k)
{ 
	int i;  
		for(i=0;i<k;i++)
		{
			printf("%3d ",a[i]);
		} 
		printf("\n");
		for(i=0;i<k;i++) 
		{
			printf("%3d ",ind[i]);
	} 
	printf("\n");
}


void mult(int** matr, int* vect1, int* vect2, int n, int m)
{   
	int i, j;
		for(i = 0; i < n; i++)
		{
			vect2[i]=0;
			for(j = 0; j < m; j++)
			{
				vect2[i]+=matr[i][j]*vect1[j];
			}
	}
}

void multsh(int*a, int*ind, int &k, int*vect1, int*vect2, int n, int m)
{ 
	int i,i1,j,j1,t,s; 
	 	for(i=0;i<n;i++)
	 	{
			s=0;
	 		for(j=0;j<m;j++)
			{ 
	 			for(t=0;t<k;t++)
	 			{ 
	 				i1=ind[t]/m;
	 				j1=ind[t]%m;
			 		if(i==i1&&j==j1) 
			 		{
			 		s+=a[t]*vect1[j];
			 		}
	 			}
				vect2[i]=s;
			}
	}
}
