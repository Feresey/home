#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct pipl
{
	char sur[80];
	char ini[2];
	int sex;
	int math;
	int inf;
} person;

int main()
{
	FILE *in, *out;
	person man;
	if ( !(in = fopen("input.txt", "r"))) printf("error input");
	if ( !(out = fopen("output.dat", "wb"))) printf("error output");
	char *a = (char*)malloc(sizeof(char)*50);
	fscanf(in,"%s", a);
	fprintf(out, "%s",  a);
	fclose(in);
	return 0;
}	
