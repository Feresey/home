#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
void main(){
    FILE *file;
	file = fopen("input13.txt", "r");
	char letters[] = "aeiouy", inp;
	int list[6] = { 0 }, ifletter = 1;
	if (file)
		while ((inp = fgetc(file)) != EOF){
			char ch = tolower(inp);
			for (int ind = 0; ind < 6; ind++)
				if (ch == letters[ind])
					list[ind] = 1;
			}
	fclose(file);
	for (int i = 0; i < 6; i++)
		if (!list[i]){
			printf("True\n");
			ifletter = 0;
			break;
		}
	if (ifletter)
		printf("False\n");
}