#include <stdio.h>
#include <math.h>
#include <stdlib.h>

typedef double(*F)(double);

double func3(double x){
	return 0.4+atan(pow(x,0.5))-x;
}
double func1(double x){
	return x*tan(x)-1.0/3.0;
}

double funcdx1(double x){
	return tan(x)+(x/(cos(x)*cos(x)));
}

double func2(double x){
	return tan(x/2.0)-1.0/tan(x/2.0)+x;
}

double funcdx2(double x){
	return 0.5/(tan(x/2.0)*tan(x/2.0))+3.0/2.0+0.5/(cos(x/2.0)*cos(x/2.0));
}

double calc_eps(){
	double eps = 1.0;
	while (eps/2.0 + 1.0 != 1.0){
		eps/=2.0;
	}
	return eps;
}

double newton(F func, F funcdx,double a, double b, double eps){
	double x = (a + b)/2.0;
	while (fabs(func(x)/funcdx(x)) > eps){
		x -= func(x)/funcdx(x);
	}
	return x;
}

double dht(F func, double a, double b, double eps){
	double x;
	while(fabs(a-b) > eps){
		x = (a+b)/2.0;
		if (func(a)*func(x) > 0.0){
			a = x;
		}
		else{
			b = x;
		}
	}
	return x;
}

double iter(F func, double a, double b, double eps){
	double x = (a + b)/2.0;
	while(fabs(x - func(x)) > eps){
		x = func(x);
	}
	return x;
}

int main(){
	double eps = calc_eps(), a1 = 0.2, b1 = 1.0, a2 = 1.0, b2 = 1.1;
	printf("Function 1:\n");
	printf("Newton:     %.10lf | diff =  %.10lf\n", newton(func1, funcdx1, a1, b1, eps), fabs(newton(func1, funcdx1, a1, b1, eps)-0.5472));
	printf("Dihotomy:   %.10lf | diff =  %.10lf\n", dht(func1, a1, b1, eps), fabs(dht(func1, a1, b1, eps)-0.5472));
	printf("Iterations: %.10lf | diff =  %.10lf\n", iter(func1, a1, b1, eps), fabs(iter(func1, a1, b1, eps)-0.5472));
    printf("Function 2:\n");
	printf("Newton:     %.10lf | diff =  %.10lf\n", newton(func2, funcdx2, a2, b2, eps), fabs(newton(func2, funcdx2, a2, b2, eps)-1.0769));
    printf("Dihotomy:   %.10lf | diff =  %.10lf\n", dht(func2, a2, b2, eps), fabs(dht(func2, a2, b2, eps)-1.0769));
	//printf("Iterations: %.10lf | diff =  %.10lf\n", iter(func2, a2, b2, eps), fabs(iter(func2, a2, b2, eps)-1.0769));
	printf("%lf", iter(func3, 1.0,2.0,eps));
	return 0;
}
