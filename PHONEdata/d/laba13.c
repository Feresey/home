#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

void def(char s[])
{
	char letters[] = "aeiouy";
	int list[6] = { 0 };

	for (int i = 0; i < strlen(s); i++)
	{
		char ch = tolower(s[i]);

		for (int ind = 0; ind < 6; ind++)
		{
			if (ch == letters[ind])
			{
				list[ind] = 1;
			}
		}
	}
	for (int i = 0; i < 6; i++)
	{
		if (!list[i])
		{
			printf("True\n");
			break;
		}
		if (i == 5 && list[i])
		{
			printf("False\n");
		}
	}
}

int main()
{
	FILE *file;
	char s[100];

	file = fopen("input13.txt", "r");
 
	if (file) {
		while (fgets (s, 100, file) != NULL)
			def(s);
		
		fclose(file);
	}
}
