#!/bin/bash
"Arch Linux")
2658             if [[ "$no_color" != "1" ]]; then
2659                 c1=$(getColor 'light cyan') # Light
2660                 c2=$(getColor 'cyan') # Dark
2661             fi
2662             if [ -n "${my_lcolor}" ]; then c1="${my_lcolor}"; c2="${my_lcolor}"; fi
2663             startline="1"                                                                                                                     2664             logowidth="38"
2665             fulloutput=(
2666 "${c1}                   -\`                 "                                                                                                2667 "${c1}                  .o+\`                %s"                                                                                              2668 "${c1}                 \`ooo/                %s"                                                                                              2669 "${c1}                \`+oooo:               %s"
2670 "${c1}               \`+oooooo:              %s"
2671 "${c1}               -+oooooo+:             %s"
2672 "${c1}             \`/:-:++oooo+:            %s"
2673 "${c1}            \`/++++/+++++++:           %s"
2674 "${c1}           \`/++++++++++++++:          %s"
2675 "${c1}          \`/+++o${c2}oooooooo${c1}oooo/\`        %s"
2676 "${c2}         ${c1}./${c2}ooosssso++osssssso${c1}+\`       %s"
2677 "${c2}        .oossssso-\`\`\`\`/ossssss+\`      %s"
2678 "${c2}       -osssssso.      :ssssssso.     %s"
2679 "${c2}      :osssssss/        osssso+++.    %s"
2680 "${c2}     /ossssssss/        +ssssooo/-    %s"
2681 "${c2}   \`/ossssso+/:-        -:/+osssso+-  %s"
2682 "${c2}  \`+sso+:-\`                 \`.-/+oso: %s"
2683 "${c2} \`++:.                           \`-/+/%s"
2684 "${c2} .\`                                 \`/%s")
2685         ;;
