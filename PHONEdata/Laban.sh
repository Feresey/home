#!/bin/bash
length=10
if [[ $(echo $2 | cut -d '=' -f 1) -eq 'length' ]]
then
	length=$2
fi
for i in $(ls $1*)
do
	if [[ $(echo -n $i | head -c $length) -eq "$i" ]]
	then
		rm -r $i
	fi
done

