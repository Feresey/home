#!/usr/bin/env python
# -*- coding: utf-8 -*-
from turtle import *




def f(x, c1, c2):
    speed( 100 );
    down()
    color( c1 );
    begin_fill()
    circle( x / 2, 180 );
    circle( x, 180 );
    left( 180 );
    circle( -x / 2, 180 );
    end_fill()
    color( c2 );
    begin_fill();
    circle( x / 2, 180 );
    left( 180 );
    circle( -x, 180 );
    circle( -x / 2, 180 )
    end_fill();
    right( 90 );
    up();
    color( c1 );
    begin_fill();
    forward( x * 0.375 )
    left( 90 );
    circle( -x / 8 );
    end_fill()
    left( 90 );
    forward( 0.75 * x );
    right( 90 );
    color( c2 );
    begin_fill()
    circle( x / 8 );
    end_fill();
    left( 90 );
    forward( 0.625 * x )
    color( 'black' );
    right( 90 )


x = 1.2


def main(args):
    reset();
    ht();
    width( 1 );
    up();
    speed( 40 );
    right( 90 );
    forward( 150 * x );
    left( 90 );
    down()
    begin_fill()
    circle( 100 * x, 180 )
    left( 60 );
    circle( -100 * x, 180 )
    left( 180 );
    circle( 200 * x, 120 )
    end_fill()
    color( 'green' )
    begin_fill()
    circle( 100 * x, 180 )
    right( 60 );
    circle( -100 * x, 180 );
    circle( -200 * x, 120 )
    end_fill()
    color( 'black' );
    circle( -200 * x );
    up()
    right( 90 );
    forward( 100 * x );
    right( 90 );
    f( 25 * x, 'white', 'green' )
    left( 90 );
    forward( 75 * x );
    left( 60 ), forward( 100 * x );
    right( 90 )
    f( 25 * x, 'black', 'green' );
    right( 90 );
    forward( 125 * x )
    left( 60 );
    forward( 100 * x );
    left( 90 );
    f( 25 * x, 'white', 'black' )
    mainloop()


if __name__ == '__main__':
    import sys

    sys.exit( main( sys.argv ) )
