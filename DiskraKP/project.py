#!/usr/bin/env python
# -*- coding: utf-8 -*-
res = []


def is_same(a, b):
    x = []
    if len( a ) == len( b ):
        for i in range(len(a)):
            if a[0] == b[i]:
                if a == (b[i:]+b[:i]):
                    return True
    return False


def compile():
    global res
    for i in range( len( res ) ):
        for j in range( len( res )-1, i, -1 ):
            if is_same( res[i], res[j] ):
                del(res[j])
    return res


def kntr(A: list, lst: list):

    for i in range( len( A ) ):
        if (A[lst[-1]][i] != 0) and (i == lst[0]):  # Если контур замкнулся
            res.append( lst )


        if (A[lst[-1]][i] != 0) and (not i in lst) and (i != lst[-1]):  # Если можно добавить элемент в контур
            kntr( A, lst + [i] )


def prnt(Matrix):
    for i in Matrix:
        print( *i, sep=' ', end='\n' )


def main(args):
    A = []
    inp = open( 'inp' )
    for line in inp:
        A.append( [int( i ) for i in line.split( ' ' )] )
    prnt( A )

    for i in range( len( A ) ):
        kntr( A, [i] )

    print( compile() )
    return 0


if __name__ == '__main__':
    import sys

    sys.exit( main( sys.argv ) )
