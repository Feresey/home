#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tkinter import *
res = []


def find():
    global text
    global text2
    global res

    text2.configure(state='normal')
    text2.delete( "0.0", END )

    Arr = list( text.get( '1.0', 'end' ).split( '\n' ) )
    if Arr == ['', '']:
        text2.insert( "1.0", "Нет матрицы :(" )
        text2.configure( state='disabled' )
        return 1
    A = []
    res = []

    for i in Arr:
        try:
            if i.split():
                A.append( [int( i ) for i in i.split()] )  # Создание матрицы из введенного текста
        except ValueError:
            text2.insert( "1.0", "Элемент матрицы не является целым числом" )
            text2.configure( state='disabled' )
            return 1

    for i in range(len(A)):
        if len(A) != len(A[i]):
            text2.insert("1.0",'Данная матрица не является матрицей смежности')
            text2.configure( state='disabled' )
            return 1

    for i in range( len( A ) ): kntr( A, [i] )  # Поиск контуров
    isk = False
    for i in compile():
        if len(i) > 2:
            x = 'V'
            for j in i:
                x += str( j + 1 ) + '→Ⅴ'
            text2.insert( "1.0", x + str( i[0] + 1 ) + '\n' )
            isk = True
    if not isk: text2.insert("1.0", "Нет контуров :(")
    text2.configure( state='disabled' )


def is_same(a, b):
    if len( a ) == len( b ):
        for i in range( len( a ) ):
            if a[0] == b[i] and a == (b[i:] + b[:i]): return True
    return False


def compile():
    global res
    for i in range( len( res ) ):
        for j in range( len( res ) - 1, i, -1 ):
            if is_same( res[i], res[j] ):
                del (res[j])
    return res


def kntr(A: list, lst: list):
    for i in range( len( A ) ):
        if (A[lst[-1]][i] != 0) and (i == lst[0]):  # Если контур замкнулся
            res.append( lst )
        if (A[lst[-1]][i] != 0) and (not i in lst) and (i != lst[-1]):  # Если можно добавить элемент в контур
            kntr( A, lst + [i] )


def exit_root(): root.destroy()


def clear():
    text2.configure( state='normal' )
    text.delete( "0.0", END )
    text2.delete( "0.0", END )
    text2.configure( state='disabled' )


root = Tk()

root.tk.call("wm", 'iconphoto', root._w, PhotoImage(file="octopi.png"))
root.geometry( "685x294" )
root.configure( bg="#FFF8DC" )
root.title( "Перечисление контуров орграфа" )

text2 = Text( root, width=33, height=13, bg="#D2B48C", wrap=WORD, relief='ridge', state='disabled' )
text = Text( root, width=33, height=13, bg="#FFF", wrap=WORD, relief='ridge')


def main(args):
    global text
    global text2

    coloration = "#D3D3D3"

    start = Button( root, text="Посчитать", width=26, command=find, bg="#D3D3F0" )
    stop = Button( root, text="Выход", width=20, command=exit_root, bg=coloration )
    clearall = Button( text="Очистить поля", width=26, command=clear, bg=coloration )

    lbl1 = Label( root, width=24, text="Введите таблицу смежности:", bg="#FFF8DC", font="Times=12", bd=3 )
    lbl2 = Label( root, width=26, text="Найденные контуры:", bg="#FFF8DC", font="Times 14", bd=3 )

    start.grid( row=2, column=1 )
    clearall.grid( row=2, column=0 )
    lbl1.grid( row=0, column=0 )
    lbl2.grid( row=0, column=1 )
    text.grid( row=1, column=0 )
    text2.grid( row=1, column=1 )
    stop.grid( row=4, column=4 )
    text.focus()
    root.mainloop()

    return 0


if __name__ == '__main__':
    import sys
    sys.exit( main( sys.argv ) )
