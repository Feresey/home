#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int N = 1000, ind = 1;
typedef struct dk
{
  int *arr;
  int start;
  int end;
} deck;

int isEmpty (deck * X);

void
prntDc (deck * X)
{
  if (!isEmpty (X))
    {
      for (int i = X->start; i <= X->end; i++)
	printf ("%d ", X->arr[i]);
      puts ("");
    }
  else
    puts ("Can't print deck: deck is empty");
  return;
}

deck *
init ()
{
  deck *X = (deck *) malloc (sizeof (deck));
  X->arr = (int *) malloc (sizeof (int) * N);
  X->start = N / 2;
  X->end = N / 2 - 1;
  return X;
}

deck *
pushStart (deck * X, int value)
{
  X->arr[X->start - 1] = value;
  X->start -= 1;
  return X;
}

deck *
pushEnd (deck * X, int value)
{
  X->arr[X->end + 1] = value;
  X->end += 1;
  return X;
}

int
isEmpty (deck * X)
{
  if (X->start == X->end + 1)
    return 1;
  return 0;
}

int
popStart (deck * X)
{
  ind = 1;
  if (isEmpty (X))
    {
      puts ("Can't popStart: Deck is empty");
      ind = 0;
      return 1;
    }
  return X->arr[X->start++];
}

int
popEnd (deck * X)
{
  ind = 1;
  if (isEmpty (X))
    {
      puts ("Can't popEnd: Deck is empty");
      ind = 0;
      return 0;
    }
  return X->arr[X->end--];
}

int
main ()
{
  deck *D = init ();
  int pop = 0;
  char q[2];
  int value = 0;

  while (scanf ("%s%d", q, &value) == 2)
    {
      if (q[0] == '+')
	{
	  if (q[1] == 's')
	    {
	      pushStart (D, value);
	    }
	  else if (q[1] == 'e')
	    {
	      pushEnd (D, value);
	    }
	}
      else if (q[0] == '-')
	{
	  if (q[1] == 'e')
	    {
	      popEnd (D, value);
	    }
	  else if (q[1] == 's')
	    {
	      popStart (D, value);
	    }
	}
      else if (q[0] == 'sf')
	{

	}
      else if (q[0] == 'se')
	{

	}
    }

  //~ D = pushStart(D, 600);
  //~ D = pushStart(D, 80000);
  //~ D = pushEnd(D, 1);
  //~ D = pushEnd(D, -11);
  //~ printf("Deck: ");
  //~ prntDc(D);

  //~ pop = popStart(D);
  //~ if (ind) 
  //~ {
  //~ printf("popS = %d\t\t", pop);
  //~ prntDc(D);
  //~ }

  //~ pop = popStart(D);
  //~ if (ind) 
  //~ {
  //~ printf("popS = %d\t\t", pop);
  //~ prntDc(D);
  //~ }

  //~ pop = popEnd(D);
  //~ if (ind) 
  //~ {
  //~ printf("popE = %d\t\t", pop);
  //~ prntDc(D);
  //~ }

  //~ pop = popEnd(D);
  //~ if (ind) 
  //~ {
  //~ printf("popE = %d\t\t", pop);
  //~ prntDc(D);
  //~ }

  //~ pop = popEnd(D);
  //~ if (ind) 
  //~ {
  //~ printf("popE = %d\t\t", pop);
  //~ prntDc(D);
  //~ }
  return 0;
}
