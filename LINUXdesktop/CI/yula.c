#include <stdio.h>
//#include <stdlib.h>
#include <string.h>
#include <malloc.h>

typedef struct TTree {
    int value;
    struct TTree* child;
    struct TTree* sibling;
} Tree;

Tree* createNode(int value) {
    Tree* node = (Tree*)malloc(sizeof(Tree));
    node->value = value;
    node->child = NULL;
    node->sibling = NULL;
    return node;
}

Tree* createRoot(int value) {
    return createNode(value);
}

Tree* addSibling(Tree* tree, int value) {
    if(tree->sibling == NULL){
        tree->sibling = createNode(value);
        return tree;
    }
    tree->sibling = addSibling(tree->sibling, value);
    return tree;
}

Tree* addChild(Tree* tree, int value) {
    if(tree->child == NULL){
        tree->child = createNode(value);
        return tree;
    }
    tree->child = addSibling(tree->child, value);
    return tree;
}

Tree* insert(Tree* tree, const char* path, int value) {
    if(*path == '\0'){
        addChild(tree, value);
        return tree;
    }
    if(*path == 'c')
        {tree->child = insert(tree->child, path + 1, value);}
    else
        {tree->sibling = insert(tree->sibling, path + 1, value);}
    return tree;
}

void printTree(const Tree* tree, int tab) {
    if(tree == NULL)
        {return;}
    for(int i = 0; i < tab; ++i)
        {putchar(' ');}
    printf("%d\n", tree->value);
    printTree(tree->child, tab + 2);
    printTree(tree->sibling, tab);
}


int deep(Tree* tree){
    int count = 0;
    while ( tree->child != NULL){
		count++;
		tree = tree->child;
	}
	return count;
}

void sheet(const Tree* tree, int deep, int count) {
	if(count > deep)
    {
        printf("NO");
        return;
    }
    if(tree == NULL)
		{return;}
    sheet(tree->sibling, deep, count);
    sheet(tree->child, deep, count++);
    
    
}

int main() {
    Tree* root = NULL;
    int value = 0;
    char q[10];
    while(scanf("%s%d", q, &value) == 2){
        if(q[0] == 'r')
            {root = createRoot(value);}
        else if(q[0] == '+'){
            root = insert(root, q + 1, value);
            puts("---------------");
            printTree(root, 0);
            puts("---------------");
        }
        else if(q[0] == 'o'){
            sheet(root, deep(root), 0);
        }
    }
}
