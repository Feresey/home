#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define KEY "%c\n"

#define T char

#include "stack.h"

// #ifdef KEY  
// #undef KEY
// #endif 

// // #define KEY "%f\n"

// #ifdef T  
// #undef T  
// #endif 

typedef struct TTree{ // деревяшка
	char value;
	struct TTree *left;
	struct TTree *right;	
}Tree;

#include "stackTreePointer.h"

// #define T Tree*

// #include "stack.h"

#define MAX_LENGTH 200

Tree* CreateTree(char value){ // создание деревяшки
	Tree* res = (Tree*)malloc(sizeof(Tree));
	res->value = value;
	res->left = NULL;
	res->right = NULL;
	return res;
}

bool IsOperand(char ch){ // проверка является ли операндом
	return ((ch >= 'a') && (ch<= 'z')) || ((ch >= 'A') && (ch <= 'Z')); 
}

bool IsOperator(char ch){ // проверка является ли оператором
	return ch == '+' || ch == '-' || ch == '*' || ch == '/';
}

int Priority(char ch){ // приоритет операций
	if(ch == '+' || ch == '-')
		return 2;
	if(ch == '*' || ch == '/')
		return 3;
	if(ch == '(')
		return 1;
}

void PrepareArray(char* a, int length){ // зануление строки
	for(int i = 0; i<length; ++i){
		*(a+i) = 0;
	}
}

void Dispose(Tree *tree){
	if(tree == NULL)
		return;
	// Dispose(tree->left);
	// Dispose(tree->right);
	free(tree);
}

void tree2exp(Tree *tree, char *ch, int *index){
	// printf("%d\n", *index);
	if(tree == NULL)
		return;
	tree2exp(tree->left, ch, index);
	*(ch + (*index)) = tree->value;
	(*index)++;
	tree2exp(tree->right, ch, index);
}

Tree* exp2tree(char* exp){ //выражение в дерево
	stackOfTree *st = NULL;
	Tree* mainTree = NULL;
	int i = 0;

	while(*(exp + i) != 0){
		char temp = *(exp + i);
		i++;

		if(IsOperand(temp)){ // если операнд, то когда оба детеныша заняты, попаем
		Tree* tr = CreateTree(temp);
			Tree* topSt = Top(st);
			if(topSt->right == NULL){
				topSt->right = tr;
			}else{
				topSt->left = tr;
				mainTree = Pop(&st);
			}
		}
		if(IsOperator(temp)){ //если оператор, то добавляем в стек
			Tree* tr = CreateTree(temp);
			if(IsEmpty(st)){
				Push(&st, tr);
				continue;
			}
			Tree* topSt = Top(st);
			if(topSt->right == NULL){
				topSt->right = tr;
				Push(&st, tr);
			}else{
				topSt->left = tr;
				Push(&st, tr);
			}
		}
	}
	while(!IsEmpty(st))
		mainTree = Pop(&st);
	return mainTree;
}

int BrackCount(char* a, int length){ //может и не нужна
	for (int i = 0; i < length; ++i)
	{
		if(*(a+i) == '(' || *(a+i) == ')' )
			length--;
	}
	return length;
}

void ReverseString(char* a){ // отражение строки
	int length = 0;
	while(*(a+length) != 0){
		length++;
	}
	for (int i = 0; i <= length/2; ++i)
	{
		char temp = *(a+i);
		*(a+i) = *(a+length-i-1);
		*(a+length-i-1) = temp;
	}
}
void printInorder(const Tree* tree, int tab) { // вывод дерева ЛКП
	if(tree == NULL){
    	return;
 	}
  	printInorder(tree->left, tab + 2);
  	for(int i = 0; i < tab; ++i){
    	putchar(' ');
  	}
  	printf("%c\n", tree->value);
  	printInorder(tree->right, tab + 2);
}

void inf2psf(char* in, char* out){ // инфиксную в постфиксную
	stack_char* st = NULL;
	int i = 0;
	int index = 0;
	
	while(*(in+i) != 0){

		char t = *(in+i);

		if(IsOperand(t)){ // Если операнд, помещаем в выходной массив
			// printf("OPERAND!\n");
			*(out+index) = t;
			index++;
		}
		if(t == '(') // Если открывающая скобка, помещаем в стек
		{
			Push_char(&st, t);
		}
		if(t == ')') // Если закрывающая скобка, помещаем в стек
		{	
			while(Top_char(st) != '('){
				*(out + index) = Pop_char(&st);
				index++;
			}
			
			Pop_char(&st);
		}
		if(IsOperator(t)){ // Если оператор, в стек, а дальше согласно приоритету операций
			while(Priority(Top_char(st)) >= Priority(t)){
				*(out + index) = Pop_char(&st);
				index++;
			}
			if(IsEmpty_char(st) || Priority(Top_char(st)) < Priority(t)){
				Push_char(&st, t);
			}
		}
		i++;
	}
	if(!IsEmpty_char(st)){
		while(!IsEmpty_char(st)){
			*(out + index) = Pop_char(&st);
			index++;
		}
	}
}

void operation(Tree **tree){ // преобразование выражения вида a*(b-c) в выражение вида a*b-a*c
	Tree *left = (*tree)->left;
	Tree *right = (*tree)->right;

	*tree = CreateTree('-');
	(*tree)->left = CreateTree('*');
	(*tree)->right = CreateTree('*');
	if(left->value == '-'){
		(*tree)->left->left = left->left;
		(*tree)->left->right = right;
		(*tree)->right->left = left->right;
		(*tree)->right->right = right;
	}else{
		(*tree)->left->left = left;
		(*tree)->left->right = right->left;
		(*tree)->right->left = left;
		(*tree)->right->right = right->right;
	}
	// printf("==================\n");

	// printInorder(*tree, 0);
}

void Walk(Tree **tree){ // обход ЛКП
	// printf("HERE!\n");
	// printf("==================\n");
	// printInorder(*tree, 0);
	if(*tree == NULL)
		return;
	Walk(&((*tree)->left));
	if((*tree)->value == '*' && ((*tree)->left->value == '-' || (*tree)->right->value == '-' )){ //Действия над деревом
		operation(tree);
		// return;
	}
	Walk(&((*tree)->right));
}

int Exists(Tree *tree){ // Предикат наличия подобного выражения в дереве

	int res = 0;

	if(tree == NULL)
		return 0;
	if(tree->value == '*' && (tree->left->value == '-' || tree->right->value == '-' )){ //Действия над деревом
		return 1;
	}
	res = Exists(tree->left);
		if(res)
			return res;
	res = Exists(tree->right);
		return res;
}

int main(){
	// char arr[MAX_LENGTH];
	char in[MAX_LENGTH];
	char out[MAX_LENGTH];
	char final[MAX_LENGTH];
	int index = 0;
	// PrepareArray(arr, MAX_LENGTH);
	PrepareArray(in, MAX_LENGTH);
	PrepareArray(out, MAX_LENGTH);
	PrepareArray(final, MAX_LENGTH);

	scanf("%s", in);
	printf("%s\n", in);

	inf2psf(in,out);

	printf("%s\n", out);

	ReverseString(out);

	printf("%s\n", out);

	Tree *tree = exp2tree(out);
	printf("%d\n", tree == NULL);
	printInorder(tree, 0);

	printf("%d\n", Exists(tree));
	printf("%d\n", Exists(tree));

	while(Exists(tree)){
		Walk(&tree);
	}

	printf("----------\n");

	printInorder(tree, 0);

	int i = 0;

	tree2exp(tree, final, &i);

	printf("%s\n", final);
	printf("%s\n", in);

}