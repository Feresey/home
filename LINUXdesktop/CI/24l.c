#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

typedef struct TTree {
	char* value;
	struct TTree* child;
	struct TTree* sibling;
} Tree;

Tree* createNode(char* value) {
	Tree* node = (Tree*)malloc(sizeof(Tree));
	node->value = (char*)malloc(100);
	//printf("%s\n", value);
	node->value = value;
	node->child = NULL;
	node->sibling = NULL;
	return node;
}

Tree* addSibling(Tree* tree, char* value) {
	if(tree->sibling == NULL){
		tree->sibling = createNode(value);
		return tree;
	}
	tree->sibling = addSibling(tree->sibling, value);
	return tree;
}

Tree* addChild(Tree* tree, char* value) {
	if(tree->child == NULL){
		tree->child = createNode(value);
		return tree;
	}
	tree->child = addSibling(tree->child, value);
	return tree;
}

void printTree(const Tree* tree, int tab) {
	if(tree == NULL){
		return;
	}
	for(int i = 0; i < tab; ++i)
		{putchar(' ');}
	printf("%s\n", tree->value);
	printTree(tree->child, tab + 2);
	printTree(tree->sibling, tab);
}

void ins(Tree* root){
	int j, start = 0, end = 0;
	unsigned int n = 100;
	char* a;
	char* res;
	a = (char*)malloc(n);
	res = (char*)malloc(n);
	scanf("%s",a);

	for(int i = 0; 1; i++){
		if( a[i] == '+' || a[i] == '\0'){
			end = i;
			j = 0;
			for(int l = start; l != end; l++)
				{res[j++] = a[l];}
			start = end + 1;
			puts(res);
			addChild(root, res);
			free(res);
			res = (char*)malloc(n);
		}
		if(a[i] == '\0')
			{break;}


	}
	free(a);
	free(res);
}




int main(){
	Tree* root = createNode("+");
	//printTree(root, 2);
	ins(root);
	printTree(root, 2);

	return 0;
}
