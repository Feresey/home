#include <stdio.h>
#include <stdlib.h>

/* 
 * typedef smth1 smth2; - позволяет использовать тип smth2 вместо типа smth1
 * В даном случае struct _tree переопределяется как tree для удобства.
 */

typedef struct _tree {
	struct _tree **chld;
	struct _tree *prnt;
	int noc;
	double value;
} tree;

/* 
 * Прототипы функций. Могут быть вынесены в отдельный
 * .h файл вместе с объявлением структуры
 *
 * Описание функций:
 *	new_tree  : выделяет память, возвр. указатель на неё, и вызывает init_tree
 *	init_tree : инициализация дерева. без вызова этой функции значения указателей и значения будут не инициализированы, что может вызвать ошибки
 *	add_child : добавляет ребёнка с указанным значением к указанному дереву
 *	del_child : удаляет ребёнка за номер chldn из указанного дерева
 *	prnt_tree : рекурсивно выводит дерево. чем больше отступ - тем глубже
 *	clean     : рекурсивно очищает всех детей, но НЕ само дерево, тк оно может быть выделенно автоматически, как в примере ниже
 */

tree *new_tree(double value);
tree *init_tree(tree * root, double value);
tree *add_child(tree * root, double value);
tree *del_child(tree * root, int chldn);
void prnt_tree(tree * root, int depth);
void clean(tree * root);

/* Реализация функций */

tree *init_tree(tree * root, double value)
{
	/* 
	 * Если malloc() не смог выделить память, 
	 * то root == NULL
	 * и обращение к полям взовет краш 
	 */
	if (root) {
		root->noc = 0;
		root->chld = NULL;
		root->prnt = NULL;
		root->value = value;
	}

	return root;
}

tree *new_tree(double value)
{
	tree *root = malloc(sizeof(tree));

	init_tree(root, value);

	return root;
}

tree *add_child(tree * root, double value)
{
	root->chld = realloc(root->chld, sizeof(tree) * ++(root->noc));

	if (root->chld) {
		tree *tmp = new_tree(value);

		root->chld[root->noc - 1] = tmp;

		tmp->prnt = root;
	}

	return root->chld[root->noc - 1];
}

tree *del_child(tree * root, int chldn)
{
	if (root && root->noc > chldn) {
		tree *del = root->chld[chldn];

		clean(del);

		free(del);

		/* 
		 * Достаточно тупой метод "убрать" ребёнка
		 * любая функция, применённая к NULL, должна будет
		 * завершиться, ничего не сделав, что спасёт от крашей.
		 * В то время как вызов например prnt_tree() от уже
		 * очищенного ребёнка вызовет падение программы.
		 * В иделае, стоит сдвинуть все элементы массива
		 * на 1 влево, начиная с chldn + 1, и уменьшить
		 * noc на 1, т.о. реально уменьшится кол-во детей.
		 * realloc() при этом можно не делать, хоть это и оставит 
		 * болтаться лишнюю память. Но так быстрее.
		 */
		root->chld[chldn] = NULL;
	}

	return root;
}

void clean(tree * root)
{
	if (root) {
		int i;

		for (i = 0; i < root->noc; ++i)
			del_child(root, i);

		free(root->chld);
		root->chld = NULL;
	}
}

void prnt_tree(tree * root, int depth)
{
	if (root) {
		int i;

		for (i = 0; i < depth; ++i)
			printf("  ");

		printf("%g\n", root->value);

		for (i = 0; i < root->noc; ++i)
			prnt_tree(root->chld[i], depth + 1);
	}
}

/* Тестовая программа */

int main(void)
{
	tree root;
	double value;

	scanf("%lg", &value);

	init_tree(&root, value);

	/* Прервётся если встречен конец файла или не число */
	while (scanf("%lg", &value) == 1)
		add_child(&root, value);

	if (root.noc > 2)
		del_child(&root, 2);

	prnt_tree(&root, 0);

	clean(&root);

	return 0;
}
