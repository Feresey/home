#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int i;
char ch;
int ind = 0;

typedef struct node
{
	char data;
	struct node *left, *right;
} Tree;

void erase(Tree* tree)
{
	if(tree != NULL)
	{
		erase(tree->left);
		erase(tree->right);
		tree->data = '\0';
		free(tree);
	}
	return;
}

void printTree(Tree* t)
{
	static int l = 0;
	l++;
	if (t && t->data != '\0')
	{
		if (t->right )
			if (t->right->data != '\0')
				printTree(t->right);
		for (i = 0; i < l; i++)printf("    ");
		printf("%c", t->data);
		if(t->right && t->left)printf("<\n");
		else printf("\n");
		printTree(t->left);
	}
	l--;
} 


int isAN()
{
	return ((ch >= 'a') && (ch <= 'z')) || ((ch >= '0') && (ch <= '9'));
}

int isN(char c)
{
	return (c >= '0') && (c <= '9');
}

int isA(char c)
{
	return  (c >= 'a') && (c <= 'z');
}

Tree* mknode(char c, Tree* l, Tree* r)
{
	Tree* t = (Tree*)malloc(sizeof(Tree));
	t->data = c;
	t->left = l;
	t->right = r;
	return t;
}

Tree* expr();

Tree* fact()
{
	Tree* t;
	t = 0;
	scanf("%c", &ch);
	if (ch == '(')
	{
		t = expr();
		if (ch != ')') printf("ERROR: not )\n");
	}
	else if (isAN(ch)) t = mknode(ch, NULL, NULL);
	else printf("ERROR: not AN\n");
	return t;
}

Tree* term()
{
	Tree* tm;
	int done;
	char ch1;
	tm = fact();
	done = 0;
	while ((ch != '\n') && (!done))
	{
		scanf("%c", &ch);
		if ((ch == '*') || (ch == '/'))
		{
			ch1 = ch;
			tm = mknode(ch1, tm, fact());
		}
		else done = 1;
	}
	return tm;
}

Tree* expr()
{
	Tree* ex;
	int done;
	char ch1;
	ex = term();
	done = 0;
	while ((ch != '\n') && (!done))
	{
		if ((ch == '+') || (ch == '-'))
		{
			ch1 = ch;
			ex = mknode(ch1, ex, term());
		}
		else done = 1;
	}
	return ex;
}

void Tree2expr(Tree* tree)
{
	if (tree)
	{
		if ((tree->data == '*') || (tree->data == '/')) printf("(");
		Tree2expr(tree->left);
		printf("%c", tree->data);
		Tree2expr(tree->right);
		if ((tree->data == '*') || (tree->data == '/')) printf(")");
	}
}

void printall(Tree* tree)
{
	printTree(tree);
	printf("\n\n-----------------------\n\n");
	Tree2expr(tree);
	printf("\n\n-----------------------\n\n");
	return;
}

void finder(Tree* tree);

void killer(Tree* tree)
{
	if (tree && tree->right)
	{
		if(tree->data != '*')
		{
			finder(tree);
		}
		else if(tree->right->data == '0' || tree->left->data == '0')
		{
			ind = 1;
			erase(tree);
		}
		else
		{
			killer(tree->right);
			killer(tree->left);
		}
	}
	return ;
}

void finder(Tree* tree)
{
	if(tree){
		if (tree->data != '*')
		{
			finder(tree->right);
			finder(tree->left);
		}
		else
		{
			ind = 0;
			killer(tree);
			if (ind)
			{
				erase(tree);
			}
			
		}
	}
	return;
}

int main()
{
	printf("Input expression:\n");
	Tree *root = expr();
	printall(root);
	finder(root);
	printall(root);
	erase(root);
	return 0;
}
