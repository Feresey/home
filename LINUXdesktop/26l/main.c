#include "deck.h"

void quickSort(deck *X, int left, int right)
{
	int pivot;
	int l_hold = left;
	int r_hold = right;
	
	pivot = X->arr[left];
	
	while (left < right)
	{
		while ((X->arr[right] >= pivot) && (left < right))
			right--;
		if (left != right)
		{
			X->arr[left] = X->arr[right];
			left++;
		}
		while ((X->arr[left] <= pivot) && (left < right))
			left++;
		if (left != right)
		{
			X->arr[right] = X->arr[left];
			right--;
		}
	}
	
	X->arr[left] = pivot;
	
	pivot = left;
	left = l_hold;
	right = r_hold;
	
	if (left < pivot)
		quickSort(X, left, pivot - 1);
	if (right > pivot)
		quickSort(X, pivot + 1, right);
}

int main()
{
	puts("Commands to create deck:\n\
	\t+s INT\t :add element to start\n\
	\t+e INT\t :add element to end\n\
	\t-s\t :pop element from start\n\
	\t-e\t :pop element from end\n\
	\t-a\t :clear deck\n\
	\tss\t :show first element(without deleting)\n\
	\tse\t :show last element (without deleting)\n\
	\tsa\t :show deck");
	
	deck *X = createDeck();
	
	printf("Your deck:\t");
	prntDc(X);
	
	quickSort(X, X->start, X->end);
	
	printf("Sorted deck:\t");
	prntDc(X);
	
	free(X->arr);
	free(X);
	return 0;
}
