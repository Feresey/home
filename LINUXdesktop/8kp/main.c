#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int size = 0;

typedef struct ring {
	int value;
	struct ring* next;
} list;

list* find(list* X, int index)
{
	list* temp = X;
	for (int i = 0; i != index; i++)
		temp = temp->next;
	return temp;
}

list* initList(int val)
{
	size++;
	list* X = (list*)malloc(sizeof(list));
	X->value = val;
	X->next = X;
	return X;
}

list* addElement(list* X, int val)
{
	if (!X) return initList(val);
	size++;
	list *temp, *point;
	temp = (list*)malloc(sizeof(list));
	point = X->next;
	X->next = temp;
	temp->value = val;
	temp->next = point;
	return temp;
}

list* delElement(list* X)
{
	size--;
	list* temp = X;
	while (temp->next != X)
		temp = temp->next;
	temp->next = X->next;
	free(X);
	return temp;
}

void prnt(list *X)
{
	printf("List:\t");
	list* temp = X;
	do
	{
		printf("%d ", temp->value);
		temp = temp->next;
	} while (X != temp) ;
	puts("");
	return;
}

void clear(list* X)
{
	if(--size > 0)
		clear(X->next);
	if (X) free(X);
	return;
}

void change(list* X, int first, int second)
{
	list *f = X, *s = X;
	int temp;
	f = find(f, first);
	s = find(s, second);
	
	temp = f->value;
	f->value = s->value;
	s->value = temp;
	return;
}

int main()
{
	list* X = NULL;
	int input, first, second;
	char *q = (char*)malloc(sizeof(char)*10);
	printf(">> ");
	
	while(scanf("%s %d", q, &input) == 2)
	{
		if (q[0] == '+')
		{
			X = addElement(X, input);
			prnt(X->next);
		}
		else if (q[0] == '-')
		{
			if (size == 0)
			{
				puts("List is empty");
				printf(">> ");
				continue;
			}
			if (X != X->next)
			{
				X = delElement(find(X, input));
				X = find(X, abs(size - input + 1));
				prnt(X->next);
			}
			else
			{
				free(X);
				X = NULL;
				size = 0;
			}
		}
		else if (q[0] == '~')
		{
			if (size == 0)
			{
				puts("List is empty");
				printf(">> ");
				continue;
			}
			puts("Input index to change");
			scanf("%d %d", &first, &second);
			change(X, first, second);
			prnt(X->next);
		}
		else break;
		printf(">> ");
	}
	
	clear(X);
	return 0;
}
