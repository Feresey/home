/****************************************/
/*		Дек на двусвязном списке		*/
/****************************************/
#include "Deck_List_Two.h"
/*Инициализация дека*/
void initDeck(Deck *D) {
initListTwo(D);
listTwoError = deckOk;
}
/*Включение в начало дека*/
void putDeckLeft(Deck *D, deckBaseType E) {
beginPtrListTwo(D);
postPut(D, E);
}
/*Исключение из начала дека*/
void getDeckLeft(Deck *D, deckBaseType *E) {
beginPtrListTwo(D);
postGet(D, E);
}
/*Включение в конец дека*/
void putDeckRight(Deck *D, deckBaseType E) {
endPtrListTwo(D);
predPut(D, E);
}
/*Исключение из конца дека*/
void getDeckRight(Deck *D, deckBaseType *E) {
endPtrListTwo(D);
predGet(D, E);
}
/*Удаление дека*/
void doneDeck(Deck *D) {
doneListTwo(D);
}
/*Предикат: пуст ли дек*/
int isDeckEmpty(Deck *D) {
return isEmptyListTwo(D);
}
