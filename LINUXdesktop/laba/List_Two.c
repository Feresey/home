#include <stdio.h>
#include <stdlib.h>
#include "List_Two.h"
int listTwoError;
void initListTwo(ListTwo *L) {
	// Выделение памяти для левого фиктивного элемента
	L->Start = (elementTwo *) malloc(sizeof(elementTwo));
	/*Если памяти не достаточно*/
	if (!L->Start) {
		listTwoError = listTwoNoMem;
		return;
	}
	/*Иначе*/
	// Выделение памяти для правого фиктивного элемента
	L->End = (elementTwo *) malloc(sizeof(elementTwo));
	/*Если памяти не достаточно*/
	if (!L->End) {
		listTwoError = listTwoNoMem;
		free((void *) L->Start);			// Удаление левого фиктивного элемента
		L->Start = NULL;
		return;
	}
	
	L->Start->nextLink = L->End;
	L->Start->predLink = NULL;
	L->End->nextLink = NULL;
	L->End->predLink = L->Start;
	L->ptr = L->Start;
	listTwoError = listTwoOk;
}
/*Включение до рабочего указателя*/
void predPut(ListTwo *L, int E) {
	/*Если рабочий указатель установлен на начало*/
	if (L->ptr == L->Start) {
		listTwoError = listTwoBegin;
		return;
	}
	/*Иначе*/
	elementTwo *pntr = (elementTwo *) malloc(sizeof(elementTwo));		// Выделение памяти под новый элемент
	/*Если памяти не достаточно*/
	if (!pntr) {
		listTwoError = listTwoNoMem;
		return;
	}
	pntr->data = E;							// Запись информации в новый элемент
	pntr->predLink = L->ptr->predLink;		// Новый эл-т указывает слева на предыдущий
	pntr->nextLink = L->ptr;				// Новый эл-т указывает справа на текущий
	L->ptr->predLink->nextLink = pntr;		// Предыдущий эл-т указывает справа на новый
	L->ptr->predLink = pntr;				// Текущий эл-т указывает слева на новый
}
/*Включение после рабочего указателя*/
void postPut(ListTwo *L, int E) {
	/*Если рабочий указатель установлен в конец*/
	if (L->ptr == L->End) {
		listTwoError = listTwoEnd;
		return;
	}
	/*Иначе*/
	// Выделение памяти под новый элемент
	elementTwo *pntr = (elementTwo *) malloc(sizeof(elementTwo));
	/*Если памяти не достаточно*/
	if (!pntr) {
		listTwoError = listTwoNoMem;
		return;
	}
	/*Иначе*/
	pntr->data = E;							// Запись информации в новый элемент
	pntr->nextLink = L->ptr->nextLink;		// Новый эл-т указывает справа на следующий
	pntr->predLink = L->ptr;				// Новый эл-т указывает слева на текущий
	L->ptr->nextLink->predLink = pntr;		// Следующий эл-т указывает слева на новый
	L->ptr->nextLink = pntr;				// Текущий эл-т указывает справа на новый
}
/*Исключение до рабочего указателя*/
void predGet(ListTwo *L, int *E) {
	/*Если список пуст*/
	if (isEmptyListTwo(L)) return;
	/*Иначе*/
	/*Если рабочий указатель указывает на начало (левый фиктивный эл-т)*/
	if (L->ptr == L->Start) {
		listTwoError = listTwoBegin;
		return;
	}
	/*Иначе*/
	*E = L->ptr->predLink->data;			// Запись информации в переменную
	elementTwo *pntr = L->ptr->predLink;	// Временный эл-т, указывающий на удаляемый
	L->ptr->predLink = pntr->predLink;		// Текущий эл-т указывает на предшествующий удаляемому
	pntr->predLink->nextLink = L->ptr;		// Предшествующий удаляемому эл-т указывает на текущий
	free((void *) pntr);					// Удаление временного элемента
}
/*Исключение после рабочего указателя*/
void postGet(ListTwo *L, int *E) {
	/*Если список пуст*/
	if (isEmptyListTwo(L)) return;
	/*Иначе*/
	/*Если рабочий указатель указывает на конец (правый фиктивный эл-т)*/
	if (L->ptr == L->End) {
		listTwoError = listTwoEnd;
		return;
	}
	/*Иначе*/
	*E = L->ptr->nextLink->data;			// Запись информации в переменную
	elementTwo *pntr = L->ptr->nextLink;	// Временный эл-т, указывающий на удаляемый
	L->ptr->nextLink= pntr->nextLink;		// Текущий эл-т указывает на следующий за удаляемым
	pntr->nextLink->predLink = L->ptr;		// Следующий за удаляемым эл-т указывает на текущий
	free((void *) pntr);					// Удаление временного элемента
}
/*Сдвиг рабочего указателя назад*/
void movePtrListTwoLeft(ListTwo *L) {
	/*Если рабочий указатель указывает на начало*/
	if (L->ptr == L->Start) {
		listTwoError = listTwoBegin;
		return;
	}
	/*Иначе смещение указателя*/
	L->ptr = L->ptr->predLink;
}
/*Сдвиг рабочего указателя вперед*/
void movePtrListTwoRight(ListTwo *L) {
	/*Если рабочий указатель указывает на конец списка*/
	if (L->ptr == L->End) {
		listTwoError = listTwoEnd;
		return;
	}
	/*Иначе смещение указателя*/
	L->ptr = L->ptr->nextLink;
}
/*Установить рабочий указатель в начало*/
void beginPtrListTwo(ListTwo *L)
{
	while (L->ptr != L->Start) 
		movePtrListTwoLeft(L);
}
/*Установить рабочий указатель в конец*/
void endPtrListTwo(ListTwo *L) 
{
	while (L->ptr != L->End) 
		movePtrListTwoRight(L);
}
/*Удаление списка*/
void doneListTwo(ListTwo *L) {
	beginPtrListTwo(L);				// Установка рабочего указателя в начало
	int E;				// Создание временного элемента
	while (!isEmptyListTwo(L)) 	// Пока список не пуст
		postGet(L, &E);				// Извлечь элемент
	/*Удаление фиктивных элементов*/
	free((void *) L->Start);
	free((void *) L->End);
}
/*Предикат: пуст ли список*/
int isEmptyListTwo(ListTwo *L) {
	if (L->Start->nextLink == L->End) {
		listTwoError = listTwoEmpty;
		return 1;
	}
	return 0;
}
int main()
{
	ListTwo *deck;
	initListTwo(deck);
}
