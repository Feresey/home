/****************************************/
/*		Дек на двусвязном списке		*/
/****************************************/
#ifndef _DECK_LIST_TWO_H_
#define _DECK_LIST_TWO_H_
#include "List_Two.h"
/*Описание исключительных ситуаций*/
const int deckOk = listTwoOk;
const int deckNoMem = listTwoNoMem;
const int deckEmpty = listTwoEmpty;
/**********************************/
/*Переменная ошибок*/
extern int listTwoError;
/*Базовый тип дека*/
typedef listTwoBaseType deckBaseType;
/*Описание типа дек*/
typedef ListTwo Deck;
/*Функции работы с деком*/
void initDeck(Deck *D);							// Инициализация дека
void putDeckLeft(Deck *D, deckBaseType E);		// Включение в начало дека
void getDeckLeft(Deck *D, deckBaseType *E);		// Исключение из начала дека
void putDeckRight(Deck *D, deckBaseType E);		// Включение в конец дека
void getDeckRight(Deck *D, deckBaseType *E);	// Исключение из конца дека
void doneDeck(Deck *D);							// Удаление дека
int isDeckEmpty(Deck *D);						// Предикат: пуст ли дек
/************************/
#endif
