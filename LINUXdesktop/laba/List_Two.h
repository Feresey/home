#ifndef _LIST_TWO_H_
#define _LIST_TWO_H_

const int listTwoOk = 0;
const int listTwoEmpty = 1;
const int listTwoNoMem = 2;
const int listTwoEnd = 3;
const int listTwoBegin = 4;
extern int listTwoError;

typedef struct elementTwo 
{
	int data;
	struct elementTwo *predLink;
	struct elementTwo *nextLink;
} elementTwo;

/*Дескриптор списка*/
typedef struct ListTwo
{
	elementTwo *Start;
	elementTwo *End;
	elementTwo *ptr;
} ListTwo;

/*Функции работы со списком*/
void initListTwo(ListTwo *L);					// Инициализация списка
void predPut(ListTwo *L, int E);	// Включение до рабочего указателя
void postPut(ListTwo *L, int E);	// Включение после рабочего указателя
void predGet(ListTwo *L, int *E);	// Исключение до рабочего указателя
void postGet(ListTwo *L, int *E);	// Исключение после рабочего указателя
void movePtrListTwoLeft(ListTwo *L);			// Сдвиг рабочего указателя назад
void movePtrListTwoRight(ListTwo *L);			// Сдвиг рабочего указателя вперед
void beginPtrListTwo(ListTwo *L);				// Установить рабочий указатель в начало
void endPtrListTwo(ListTwo *L);					// Установить рабочий указатель в конец
void doneListTwo(ListTwo *L);					// Удаление списка
int isEmptyListTwo(ListTwo *L);					// Предикат: пуст ли список
/***************************/
#endif
