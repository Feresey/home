#ifndef __peson_h__
#define __peson_h__

#include <stdio.h>
#include <string.h>
#include <errno.h>

typedef struct pipl 
{
	char sur[80];
	char ini[2];
	int math;
	int inf;
	char sex[1];
	int group;
} person;

void title(FILE* x)
{
	fprintf(x,"┌─────────────┬──────────┬──────┬───────┬─────┬───────┐\n");
	fprintf(x,"│  Surname    │ Initials │ Math │  Inf  │ Sex │ Group │\n");
	fprintf(x,"├─────────────┼──────────┼──────┼───────┼─────┼───────┤\n");
}

void print(person p, FILE* x)
{
	fprintf(x ,"│ %-11s │    %2s    │  %d   │   %.d   │  %s  │  %3d  │\n",
		p.sur,
		p.ini,
		p.math,
		p.inf,
		p.sex,
		p.group);
	return;
}

void endTile(FILE *x)
{
	fprintf(x, "└─────────────┴──────────┴──────┴───────┴─────┴───────┘\n");
}

#endif
