#!/bin/bash 
if [[ $# = 0 ]] 
then 
echo "NO argument" 
exit 2 
fi 
while [ 1 ] 
do 
	sum=0 
	for i in $(find . -name "$1"'*') 
	do 
		sum=$(($sum + $(stat -c %s $i))) 
	done 
	echo "Current size: "$sum 
	if [[ $2 -lt $sum ]] 
	then 
		rm -vi $(find . -name $1'*' -exec ls -Ss {} \; |sort -h | tail -n 1|cut -d ' ' -f 2)
	else 
		echo done 
		break 
	fi 
done
